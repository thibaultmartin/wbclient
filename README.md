# Disclaimer

This project is primarily targetted at learning Rust, and get back to
programming after a few years of PowerPoint writing.

It probably contains really bad code and I discourage you running for production
purposes.

# Goal

This library provides a client for wallabag. It handles the tokens and requests
behind the scenes so you can focus on manipulating wallabag entries