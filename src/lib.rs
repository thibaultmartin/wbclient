//! This library is meant to be used as a client for Wallabag. It handles all
//! the token retrieval and http calls logic so you only use `Entries`
//!
//! `Entries` are the way Wallabag models articles

use serde::Deserialize;
use std::collections::HashMap;

pub mod error;
use self::error::Error;
use self::error::Error::*;

#[derive(Deserialize, Debug)]
pub struct Entry {
    pub content: Option<String>,
    pub created_at: Option<String>,
    pub domain_name: Option<String>,
    pub id: u32,
    pub is_archived: u32,
    pub is_starred: u32,
    pub language: Option<String>,
    pub mimetype: Option<String>,
    pub preview_picture: Option<String>,
    pub reading_time: u32,
    pub tags: Vec<String>,
    pub title: Option<String>,
    pub updated_at: Option<String>,
    pub url: Option<String>,
    pub user_email: Option<String>,
    pub user_id: u32,
    pub user_name: Option<String>,
}

enum EntryField {
    Archive(bool),
    Starred(bool),
}

#[derive(Deserialize, Debug)]
struct TokenResponse {
    access_token: String,
    expires_in: u32,
    token_type: String,
    scope: Option<String>,
    refresh_token: String,
}

#[derive(Deserialize, Debug)]
struct EntriesResponse {
    page: u32,
    limit: u32,
    pages: u32,
    _embedded: Option<Item>
}

#[derive(Deserialize, Debug)]
struct Item {
    items: Option<Vec<Entry>>
}

pub struct Client<'a> {
    reqwest_client: &'a reqwest::Client,
    baseurl: url::Url,
    client_id: &'a str,
    client_secret: &'a str,
    username: &'a str,
    password: &'a str,
}

impl<'a> Client<'a> {
    pub fn new(reqwest_client: &'a reqwest::Client, baseurl: url::Url, client_id: &'a str, client_secret: &'a str, username: &'a str, password: &'a str) -> Client<'a> {
        Client {
            reqwest_client,
            baseurl,
            client_id,
            client_secret,
            username,
            password,
        }
    }

    /// This function retrieves an Access Token from the informations provided
    /// by the Client. It only handles ROPC flow, which is what Wallabag uses
    /// anyway.
    /// It does not handle token expiration, which means you need to retrieve a
    /// token before each API call in order to make sure your token is valid
    ///
    /// # Errors
    /// This function can fail to retrieve tokens if:
    /// * it cannot establish a connexion to the Authorization Server
    /// * any of `client_id`, `client_secret`, `username`, `password` is invalid
    /// * the Authorization Server response cannot be parsed
    fn retrieve_tokens(&self) -> Result<String, Error> {
        let mut params = HashMap::new();
        params.insert("grant_type",     "password");
        params.insert("client_id",      self.client_id);
        params.insert("client_secret",  self.client_secret);
        params.insert("username",       self.username);
        params.insert("password",       self.password);

        // Query the token endpoint of the AS
        let query_url = format!("{}oauth/v2/token", self.baseurl);
        let mut resp = self.reqwest_client.post(&query_url)
                        .form(&params)
                        .send();

        // Return an error if we cannot reach AS
        if let Err(e) = resp {
            return Err(CannotReachAS {});
        }

        // Return an Error if we cannot parse the reponse
        let mut resp = resp.unwrap();
        let token_resp: Result<TokenResponse, reqwest::Error> = resp.json();
        if let Err(e) = token_resp {
            return Err(CannotParseASResponse {});
        }
        let token_resp = token_resp.unwrap();

        Ok(token_resp.access_token)
    }

    /// This function retrieves all the existing entries on the Wallabag
    /// instance, whether they are archived, stared, or not.
    /// It handles the access token retrieval automatically from the user
    /// credentials
    ///
    /// # Example
    /// ```
    /// let client = wbclient::Client::new(...);
    /// let entries = get_all_entries()?;
    /// for entry in entries {    
    ///   println!("{} - {}", entry.id.unwrap(), entry.title.unwrap());
    /// }
    /// ```
    pub fn get_all_entries(&self) -> Result<Vec<Entry>, Error> {
        let mut entries: Vec<Entry> = vec![];
        let access_token =  self.retrieve_tokens()?;

        let resp = self.get_entries(1, &access_token)?;
        let pages = resp.pages;
        Client::parse_entries_response(resp, &mut entries);

        for i in 2..(pages+1) {
            let newresp = self.get_entries(i, &access_token)?;
            Client::parse_entries_response(newresp, &mut entries);
        }

        Ok(entries)
    }

    fn parse_entries_response(resp: EntriesResponse, entries: &mut Vec<Entry>) {
        // _embedded is a JSON field and can be null (but should not)
        if resp._embedded.is_none() { return; }
        let mut embedded = resp._embedded.unwrap();
        
        // items is a JSON field and can be null, if there is no article on the
        // Wallabag instance
        if embedded.items.is_none() {
            return;
        } else {
            let mut embeditems = embedded.items.unwrap();
            entries.append(&mut embeditems);
        }
    }

    fn get_entries(&self, page: u32, access_token: &str) -> Result<EntriesResponse, Error> {
        let query_url = format!("{}api/entries.json", self.baseurl);

        let mut resp = self.reqwest_client.get(&query_url)
               .bearer_auth(access_token.to_string())
               .query(&[("page", page)])
               .send();

        if let Err(e) = resp {
            return Err(CannotReachServer {})
        }

        let mut resp = resp.unwrap();
        let entries_response: Result<EntriesResponse, reqwest::Error> = resp.json();
        if let Err(e) = entries_response {
            return Err(CannotParseEntriesResponse {})
        }
        
        Ok(entries_response.unwrap())
    }

    pub fn get_all_entries_since(&self, since: &str) {}

    pub fn create_entry(&self, url: url::Url) -> Result<Entry, Error> {
        let access_token = self.retrieve_tokens()?;

        let mut params = HashMap::new();
        params.insert("url", url.to_string());

        let query_url = format!("{}api/entries",self.baseurl);

        let mut resp = self.reqwest_client.post(&query_url)
                       .bearer_auth(access_token)
                       .json(&params)
                       .send();

        if let Err(e) = resp {
            return Err(CannotReachServer);
        }

        let mut resp = resp.unwrap();
        if !resp.status().is_success() {
            return Err(CannotAddEntry);
        }
        let entry: Result<Entry, reqwest::Error> = resp.json();

        if let Err(e) = entry {
            return Err(CannotParseEntriesResponse);
        }

        Ok(entry.unwrap())
    }

    /// This function deletes an entry
    pub fn delete_entry(&self, id: u32) -> Result<(), Error> {
        let access_token = self.retrieve_tokens()?;
        let query_url = format!("{}api/entries/{}", self.baseurl,id);

        let mut resp = self.reqwest_client.delete(&query_url)
                       .bearer_auth(access_token)
                       .send();

        if let Err(e) = resp {
            return Err(CannotReachServer);
        }

        let mut resp = resp.unwrap();
        if !resp.status().is_success() {
            return Err(CannotDeleteEntry);
        }

        Ok(())
    }

    /// This function marks a given entry as read/archived
    pub fn mark_as_read(&self, id: u32) -> Result<(), Error> {
        self.switch_parameter(id, EntryField::Archive(true))?;
        Ok(())
    }

    /// This function marks a given entry as unread/unarchived
    pub fn mark_as_unread(&self, id: u32) -> Result<(), Error> {
        self.switch_parameter(id, EntryField::Archive(false))?;
        Ok(())
    }

    /// This internal function is meant to update a parameter of an Entry on the
    /// remote server
    fn switch_parameter(&self, id: u32, field: EntryField) -> Result<(), Error> {
        let access_token =  self.retrieve_tokens()?;

        let query_url = format!("{}api/entries/{}",self.baseurl, id);
        let mut params = HashMap::new();
        match field {
            EntryField::Archive(true) => params.insert("archive", 1),
            EntryField::Archive(false) => params.insert("archive", 0),
            EntryField::Starred(true) => params.insert("starred", 1),
            EntryField::Starred(false) => params.insert("starred", 0),
        };

        let mut resp = self.reqwest_client.patch(&query_url)
                       .bearer_auth(access_token)
                       .json(&params)
                       .send();

        if let Err(e) = resp {
            return Err(CannotReachServer);
        }

        let mut resp = resp.unwrap();
        if !resp.status().is_success() {
            // TODO handle more cleanly server responses
            return Err(EntryNotFound);
        }

        Ok(())        
    }
}