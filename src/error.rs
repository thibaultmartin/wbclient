use std::fmt;

#[derive(Debug)]
pub enum Error {
	CannotReachAS,
	CannotParseASResponse,
	CannotReachServer,
	CannotParseEntriesResponse,
	EntryNotFound,
	CannotAddEntry,
	CannotDeleteEntry,
}

impl fmt::Display for Error {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		match *self {
			Error::CannotReachAS => write!(f,"Cannot reach Authorization Server"),
			Error::CannotParseASResponse => write!(f,"Cannot parse AS response"),
			Error::CannotReachServer => write!(f, "Cannot reach walabag instance"),
			Error::CannotParseEntriesResponse => write!(f, "Cannot parse entries response"),
			Error::EntryNotFound => write!(f, "Entry not found"),
			Error::CannotAddEntry => write!(f, "Cannot add entry"),
			Error::CannotDeleteEntry => write!(f, "Cannot delete entry"),
		}
	}	
}